package com.natwest.ai.exercise.cucumber;

import io.cucumber.java.en.*;
import static org.junit.jupiter.api.Assertions.*;
import org.json.*;

import com.natwest.ai.exercise.ApiClient;

public class BillPayStepDefinitions {

  public BillPayStepDefinitions() throws Exception {
    setup();
  }

  public void setup() {}

  @Given("I have a valid API access token with ReadAccountsDetail API consent.")
  public void iHaveAValidAPIAccessTokenWithReadAccountsDetailAPIConsent() {}


  @When("I call the accounts API endpoint.")
  public void iCallTheAccountsAPIEndpoint() {}


  @Then("I receive a list of all accounts included in my Bank of APIs test data.")
  public void iReceiveAListOfAllAccountsIncludedInMyBankOfAPIsTestData() {}


  @Given("I have a valid API access token with BillPayService API consent.")
  public void iHaveAValidAPIAccessTokenWithBillPayServiceAPIConsent() {}


  @When("I call the payees API endpoint providing a valid account ID.")
  public void iCallThePayeesAPIEndpointProvidingAValidAccountID() {}


  @Then("I receive a list of all payees associated with that account ID.")
  public void iReceiveAListOfAllPayeesAssociatedWithThatAccountID() {}


  @When("I call the bill-pay-request API endpoint providing a valid payeeId ID and amount.")
  public void iCallTheBillPayRequestAPIEndpointProvidingAValidPayeeIdIDAndAmount() {}


  @Then("I receive a response containing a valid request referenceNumber.")
  public void iReceiveAResponseContainingAValidRequestReferenceNumber() {}

}
