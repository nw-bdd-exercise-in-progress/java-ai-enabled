package com.natwest.ai.exercise.junit;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.json.*;

import com.natwest.ai.exercise.ApiClient;

public class ApiClientTest {

    @BeforeEach
    public void setup() throws Exception {}

    @Test
    public void testGetAccessToken() throws Exception {}

    @Test
    public void testGrantConsent() throws Exception {}

    @Test
    public void testAuthorizeConsent() throws Exception {}

    @Test
    public void testexchangeAuthorizationCodeForAccessToken() throws Exception {}

    @Test
    public void testGetAccounts() throws Exception {}

    @Test
    public void testGetPayees() throws Exception {}

    @Test
    public void testBillPay() throws Exception {}
}
